package com.groundgurus.registrationapp.rest;

import com.groundgurus.registrationapp.entity.Registration;
import com.groundgurus.registrationapp.service.RegistrationService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Rest -> Service (Done) -> Dao (Done) -> Entity (done)
 * @author pgutierrez
 */
@RestController
@RequestMapping("/api/registration")
public class RegistrationRest {
    @Autowired
    private RegistrationService registrationService;
    
    @GetMapping("all")
    public List<Registration> findAll() {
        return registrationService.findAll();
    }
    
    @GetMapping("email")
    public Registration findOneByEmail(@RequestParam String email) {
        return registrationService.findOneByEmail(email);
    }
}
