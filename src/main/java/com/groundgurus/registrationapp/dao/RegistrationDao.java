package com.groundgurus.registrationapp.dao;

import com.groundgurus.registrationapp.entity.Registration;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

/**
 * JPQL
 * @author pgutierrez
 */
public interface RegistrationDao extends CrudRepository<Registration, Integer> {
   List<Registration> findAll();
   Registration findOneByEmail(String email);
}
