package com.groundgurus.registrationapp.service;

import com.groundgurus.registrationapp.dao.RegistrationDao;
import com.groundgurus.registrationapp.entity.Registration;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author pgutierrez
 */
@Service
public class RegistrationService {
    @Autowired
    private RegistrationDao registrationDao;

    public List<Registration> findAll() {
        return registrationDao.findAll();
    }
    
    public Registration findOneByEmail(String email) {
        return registrationDao.findOneByEmail(email);
    }
}
